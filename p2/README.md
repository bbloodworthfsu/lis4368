# LIS4368 - Advanced Web Applications Development

## Benjamin Bloodworth

### Project 2 Requirements

1. Created CRUD statements for data entry using prepared statements to protect against SQL injection.
2. Created insert form.
3. Created admin form for editing and deleting users.

#### Assignment Screenshots

*Insert Screen*:
![Insert Screen](img/ss1.png)

*Thanks Screen*
![Thanks Screen](img/ss2.png)

*List Screen*
![List Screen](img/ss3.png)

*Modify Screen*
![Modify Screen](img/ss4.png)

*Entry Modified*
![Entry Modified](img/ss5.png)

*Delete Prompt*
![Delete Prompt](img/ss6.png)

*Data Before Insertion*
![Data Before Insertion](img/ss7.png)

*Data After Insertion*
![Data After Insertion](img/ss8.png)

*Data After Edit*
![Data After Edit](img/ss9.png)

*Data After Deletion*
![Data After Deletion](img/ss10.png)