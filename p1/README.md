# LIS4368 - Advanced Web Applications Development

## Benjamin Bloodworth

### Project 1 Requirements

1. Build form using bootstrap markup to match fields from customer table in database.
2. Add validation for form fields using regular expressions.
3. Update navigation links and meta data.
4. Provide screenshot of validation success and failure.

#### Assignment Screenshots

*Failed Validation*:
![Failed validation Screenshot](img/failed-validation.png)

*Passed Validation*
![Passed Validation](img/passed-validation.png)
