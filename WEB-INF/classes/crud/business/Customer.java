package crud.business;

import java.io.Serializable;

//Reality-check: zip should be int, phone long, balance and totalSales BigDecimal data types
public class Customer implements Serializable
{
	private String id;
	private String fname;
	private String lname;
	private String street;
	private String city;
	private String state;
	private String zip;
	private String phone;
	private String email;
	private String balance;
	private String totalSales;
	private String notes;

	//default constructor
	public Customer()
	{
		id = "";
		fname = "";
		lname = "";
		street = "";
		city = "";
		state = "";
		zip = "";
		phone = "";
		email = "";
		balance = "";
		totalSales = "";
		notes = "";
	}

	public Customer(
		String parId,
		String firstName, 
		String lastName, 
		String street, 
		String city, 
		String state, 
		String zip, 
		String phone, 
		String email,
		String balance,
		String totalSales,
		String notes)
	{
		this.id = parId;
		this.fname = firstName;
		this.lname = lastName;
		this.street = street;
		this.city = city;
		this.state = state;
		this.zip = zip;
		this.phone = phone;
		this.email = email;
		this.balance = balance;
		this.totalSales = totalSales;
		this.notes = notes;
	}

/*
	Note: Java getter/setter method names must start with get/set, followed by the capitalized property name, example:
	public String getFoo() {
  return foo;
	}
*/
	
	//getter/setter methods:
	//id
	public String getId()
	{
		return id;
	}

	public void setId(String parId)
	{
		this.id = parId;
	}
	//fname
	public String getFname()
	{
		return this.fname;
	}

	public void setFname(String firstName)
	{
		this.fname = firstName;
	}

	//lname
	public String getLname()
	{
		return this.lname;
	}

	public void setLname(String lastName)
	{
		this.lname = lastName;
	}

	// street
	public String getStreet() {
		return this.street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	// city
	public String getCity() {
		return this.city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}

	// state
	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	// zip
	public String getZip() {
		return this.zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	// phone
	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	// email
	public String getEmail()
	{
		return email;
	}
	
	public void setEmail(String email)
	{
		this.email = email;
	}

	// balance
	public String getBalance() {
		return this.balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	// totalSales
	public String getTotalSales() {
		return this.totalSales;
	}

	public void setTotalSales(String totalSales) {
		this.totalSales = totalSales;
	}

	// notes
	public String getNotes() {
		return this.notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}
}
