# LIS4368 - Advanced Web Applications Development

## Benjamin Bloodworth

### Assignment 5 Requirements

*Five Parts:*

1. Created class to manage database connections.
2. Created class for adding customers to the database.
3. Created class for managing database connection pool.
4. Screenshots of successfully adding new record.
5. Cross site scripting attack filtering.

#### Assignment Screenshots

*Entry Form*:

![Failed Validation](img/ss1.png)

*Success Screen*

![Passed Validation](img/ss2.png)

*Database Values*

![Passed Validation](img/ss3.png)