# LIS4368 - Advanced Web Applications Development

## Benjamin Bloodworth

### Assignment 3 Requirements

*Three Parts:*

1. Install MySQL Workbench
2. Create Pet Store ERD
3. Forward engineer database

#### Assignment Screenshots

*Screenshot of ERD*:

![ERD Screenshot](img/a3erd.png)

*MWB File*
[MWB File](a3.mwb)

*SQL File*
[SQL File](a3.sql)