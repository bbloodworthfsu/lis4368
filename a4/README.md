# LIS4368 - Advanced Web Applications Development

## Benjamin Bloodworth

### Assignment 4 Requirements

*Three Parts:*

1. Build form using bootstrap markup to match fields from cusotmer table in database.
2. Add server side validation.
3. Update navigation links and meta data.
4. Provide screenshot of validation success and failure.

#### Assignment Screenshots

*Validation Failed*:
[Failed Validation](img/validation-failed.png)

*Validation Passed*
[Passed Validation](img/validation-passed.png)