# LIS4368 - Advanced Web Applications Development

## Benjamin Bloodworth

### Assignment 2 Requirements

*Three Parts:*

1. Install MySQL and Tomcat
2. JSP/Servlet Development
3. Chapter Questions (Chs 5 - 6)

#### README.md file should include the following items

* Assessment links (as above)
* Screenshot of the query results from the following link (see screenshot below): http://localhost:9999/hello/querybook.html


#### Assignment Screenshots

*Screenshot of Servlet Results*:

![Servlet Results Screenshot](img/servlet.png)

#### Tutorial Links

* [http://localhost:9999/hello](http://localhost:9999/hello)
* [http://localhost:9999/hello/HelloHome.html](http://localhost:9999/hello/index.html)
* [http://localhost:9999/hello/sayhello](http://localhost:9999/hello/sayhello)
* [http://localhost:9999/hello/querybook.html](http://localhost:9999/hello/querybook.html)
* [http://localhost:9999/hello/sayhi](http://localhost:9999/hello/sayhi)