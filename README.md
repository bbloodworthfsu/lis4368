# LIS4368 - Advanced Web Applications Development

## Benjamin Bloodworth

### LIS4368 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Distributed Version Control with Git and Bitbucket
    - Java/JSP/Servlet Development Installation
    - Chapter Questions (Chs 1 - 4)
2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Install MySQL
    - Install Tomcat
    - JSP Servlet Development
    - Chapter Questions (Chs 5 - 6)
    - Chapter Questions (Chs 1 - 4)
3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Install MySQL Workbench
    - Create Pet Store ERD
    - Forward engineer database
4. [P1 README.md](p1/README.md "My P1 README.md file")
    - Build form using bootstrap markup to match fields from customer table in database.
    - Add validation for form fields using regular expressions.
    - Update navigation links and meta data.
    - Provide screenshot of validation success and failure.
5. [A4 README.md](a4/README.md "My A4 README.md file")
    - Build form using bootstrap markup to match fields from cusotmer table in database.
    - Add server side validation.
    - Update navigation links and meta data.
    - Provide screenshot of validation success and failure.
6. [A5 README.md](a5/README.md "My A5 README.md file")
    - Created class to manage database connections.
    - Created class for adding customers to the database.
    - Created class for managing database connection pool.
    - Screenshots of successfully adding new record.
    - Cross site scripting attack filtering.
7. [P2 README.md](p2/README.md "My A6 README.md file")
    - Created CRUD statements for data entry using prepared statements to protect against SQL injection.
    - Created insert form.
    - Created admin form for editing and deleting users.